# from bluetooth import *
# print("performing inquiry...")
# nearby_devices = discover_devices(lookup_names = True)
# print("found %d devices" % len(nearby_devices))
# for name, addr in nearby_devices:
#     print(" %s \t %s" % (addr, name))

import sys
try:
   import queue
except ImportError:
   import Queue
import time
from time import sleep
from NeuroPy import NeuroPy
from gooey import Gooey, GooeyParser

FILE_NAME = "results.csv"
PERIOD_OF_TIME = 20 # second
COM_PORT = "COM4"

q = Queue.Queue()

def writeQueueToFile():
    print("Writing to file...")
    with open(FILE_NAME, 'a') as f:
        while not q.empty():
            item = q.get()
            f.write('%f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n' % (item[0], item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9], item[10], item[11], item[12], item[13]))

def writeHeadersToFile():
    print("Writing headers to file...")
    with open(FILE_NAME, 'w') as f:
        f.write('time,attention, meditation, rawValue, delta, theta, lowAlpha, highAlpha, lowBeta, highBeta, lowGamma, midGamma, poorSignal, blinkStrength\n')



def rawValue_callback(val):
    "this function will be called everytime NeuroPy has a new value for attention"
    
    timestamp = time.time()
    print("Value of rawData is %f - %f" % (timestamp, val))
    q.put((timestamp, neuroPy.attention, neuroPy.meditation, neuroPy.rawValue, neuroPy.delta, neuroPy.theta, neuroPy.lowAlpha, neuroPy.highAlpha, neuroPy.lowBeta, neuroPy.highBeta, neuroPy.lowGamma, neuroPy.midGamma, neuroPy.poorSignal, neuroPy.blinkStrength))

    if q.qsize() > 10000:
        writeQueueToFile()
    return None


neuroPy = NeuroPy(COM_PORT)  # If port not given 57600 is automatically assumed

def main():
    writeHeadersToFile()
    # neuroPy=NeuroPy("/dev/rfcomm0") for linux
    # set call back:
    neuroPy.setCallBack("rawValue", rawValue_callback)

    # call start method
    neuroPy.start()


    start = time.time()
    while True:
        if time.time() > start + PERIOD_OF_TIME : break

    writeQueueToFile()


@Gooey(program_name='NeuroMind')
def parse_args():
    global FILE_NAME, PERIOD_OF_TIME, COM_PORT
    parser = GooeyParser(description='Analyzing Brain Waves')
    # parser.add_argument("steps", type=int, default=15)
    # parser.add_argument("delay", type=int, default=1)
    parser.add_argument("PeriodOfTime", metavar='Period Of Time (Seconds)', type=int, default=PERIOD_OF_TIME)
    parser.add_argument("ComPort", metavar='Com Port', default=COM_PORT)
    parser.add_argument("FileName", metavar='Output File Name', default=FILE_NAME)

    args = parser.parse_args(sys.argv[1:])
    FILE_NAME = args.FileName
    PERIOD_OF_TIME = args.PeriodOfTime
    COM_PORT = args.ComPort
    # for i in range(args.steps):
    #     print("progress: {}/{}".format(i+1, args.steps))
    #     sys.stdout.flush()
    #     sleep(args.delay)
    return args

if __name__ == '__main__':
    args = parse_args()
    main()
